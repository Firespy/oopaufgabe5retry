import java.util.Iterator;

public class OrderedMap<A extends Shorter<? super A>, B> extends OrderedSet<A> {
	private MapNode head = null; // first node of list
	private MapNode tail = null; // last list node
	
	@Override 
	public MapIterator<A> iterator(){
		return new MapIterator<A>();
	}
	@Override
	public MapNode getHead(){
		return this.head;
	}
	@Override
	public MapNode getTail(){
		return this.tail;
	}
	
	private void setTail(MapNode tail){
		this.tail=tail;
	}
	
	private void setHead(MapNode head){
		this.head=head;
	}
	@Override
	public void add(A x) {
		// add element to list

		if (getHead() == null){

			setHead(new MapNode(x));
			setTail(getHead());

		}else {
			MapNode b = getHead().getNext();
			MapNode a = getHead();
			while ((a!= null) && !(a.getElem().shorter(x))) {
				a = a.getNext();
				if(b!=null)
					b = b.getNext();
			}

			MapNode xi = new MapNode(x);
			if (a == getHead()) {
				setHead(xi);
				xi.setNext(a);
				a.setNext(b);

			} 
			else if(a==null){
				MapNode bla = new MapNode(x);
				getTail().setNext(bla);
				setTail(bla);

			}else {
				Node k =getHead();
				while(k.getNext()!=a){
					k=k.getNext();
				}

				k.setNext(xi);
				xi.setNext(a);
				a.setNext(b);

			}

			//xi.getNext() = b;
			// xi.getNext()=b;
		}

	}

	private class MapNode  extends Node  {
		private Set<B> myValues = new Set<B>();

		// element in node
		private MapNode next = null; // next node in list
		public Set<B> getSet(){
			return myValues;
		}
		public MapNode getNext(){
			return next;
		}
		public void setNext(MapNode a){
			this.next=a;
		}

		protected MapNode(A elem) {
			super(elem);
			//this.elem=elem;
			// TODO Auto-generated constructor stub
		}



	}

	public class MapIterator<A> extends ListIter implements Iterable<B> {
		private MapNode b =  getHead();

		@Override
		public Iterator<B>iterator() {
			return b.myValues.iterator();
			
		}

	}
}
