public class OrderedSet<A extends Shorter<? super A>> extends Set<A>{
	private Node head = null; // first node of list
	private Node tail = null; // last list node
	@Override
	public Node getHead(){
		return this.head;
	}
	@Override
	public Node getTail(){
		return this.tail;
	}
	@Override
	public void setTail(Node tail){
		this.tail=tail;
	}
	@Override
	public void setHead(Node head){
		this.head=head;
	}
	@Override
	public void add(A x) {
		// add element to list

		if (getHead() == null){

			setHead(new Node(x));
			setTail(getHead());

		}else {
			Node b = getHead().getNext();
			Node a = getHead();
			while ((a!= null) && !(a.getElem().shorter(x))) {
				a = a.getNext();
				if(b!=null)
					b = b.getNext();
			}

			Node xi = new Node(x);
			if (a == getHead()) {
				setHead(xi);
				xi.setNext(a);
				a.setNext(b);

			} 
			else if(a==null){
				Node bla = new Node(x);
				getTail().setNext(bla);
				setTail(bla);

			}else {
				Node k =getHead();
				while(k.getNext()!=a){
					k=k.getNext();
				}

				k.setNext(xi);
				xi.setNext(a);
				a.setNext(b);

			}

			//xi.getNext() = b;
			// xi.getNext()=b;
		}

	}

}
