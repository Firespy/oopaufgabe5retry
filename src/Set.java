import java.lang.Iterable;
import java.util.Iterator;

public class Set<A> implements Iterable<A> {
	private Node head = null; // first node of list
	private Node tail = null; // last list node
	protected class Node {
		private A elem;
		// element in node
		public A getElem(){
			return elem;
		}
		public Node getNext(){
			return next;
		}
		public void setNext (Node next){
			this.next=next;
		}
		public void setElem(A elem){
			this.elem=elem;
		}
		private Node next = null; // next node in list
		protected Node(A elem) {
			setElem(elem);
		}
	}

	public Node getHead(){
		return this.head;
	}
	
	public Node getTail(){
		return this.tail;
	}
	
	public void setTail(Node tail){
		this.tail=tail;
	}
	
	public void setHead(Node head){
		this.head=head;
	}

	protected class ListIter implements Iterator<A> {
		private Node p = getHead();
		private Node tmpEl=getHead();
		private Node tmpEl2=getHead();

		//Nachbedingung: liefert den Object des listelements zurueck wenn getHead() != null sonst null und geht eins weiter

		public A next() {

			if(tmpEl!=getHead()){
				tmpEl2=tmpEl2.getNext();
			}
			if(p!=getHead()){
				tmpEl=tmpEl.getNext();

			}
			if (p == null)
				return null;
			A elem = p.getElem();
			p = p.getNext();
			return elem;
		}

		//Nachbedingung: keine Aenderung der Instanzvariablen, liefert true wenn es noch ein Element gibt sonst false

		public boolean hasNext() {
			return p != null;
		}

		//Nachbedingung: Verweise zeigen nicht mehr von/auf das aktuelle Element aus dem Set

		public void remove(){
			if(tmpEl==getHead()){

				setHead(getHead().next);
				tmpEl.next=null;
				tmpEl2=getHead();
				tmpEl=getHead();
			}
			else if(tmpEl == getTail()){
				setTail(tmpEl2);
				
				getTail().next=null;
				tmpEl.next=null;
				tmpEl=getTail();
			}
			else{
				tmpEl2.next=tmpEl.next;
				tmpEl.next=null;
				tmpEl=tmpEl2.next;
			}
		}
	}

	public void add(A x) {
		// add element to list
		if (getHead() == null){
			Node bla = new Node(x);
			setHead(bla);
			setTail(bla);
			//setHead(new)
			//getTail() = getHead() = new Node(x);
		}else{
			Node bla = new Node(x);
			getTail().setNext(bla);
			setTail(bla);
		}
		
	}
	public Iterator<A> iterator() { // new list iter.
		return new ListIter();
	}
}
