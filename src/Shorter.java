
public interface Shorter<A> {
	public boolean shorter(A That);
}
