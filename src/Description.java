public class Description implements Shorter<Description> {
	String description;

	public Description(String desc) {
		this.description = desc;
	}

	@Override
	public boolean shorter(Description That) {
		if (description.length() < That.countChars())
			return false;
		else
			return true;
	}

	@Override
	public String toString() {
		return this.description;
	}

	public int countChars() {
		return description.length();
	}

}
